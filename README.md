This is a basic Todo application made in Unity using C#.

This project is implements the basic principles and practices of Redux and Reactive Programming.

This project also implements the concept of Thunk Middlewares for Redux.

The Redux store is based on reduxity for Redux, redux.NET-thunk for Thunk Middleware and UniRx plugin is directly used for Reactive Programming.