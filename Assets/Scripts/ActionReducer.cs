﻿using UnityEngine;

namespace Demo.ReduxStore
{
    /// <summary>
    /// This is a class for actions
    /// </summary>

    public interface IAction
    {
    }

    public class Action
    {
        public class Increment : IAction { }
        public class Decrement : IAction { }
        public class AddTodo : IAction { }
        public class DeleteTodo: IAction { }
        public class ChangeListViewState : IAction { }

        public class ChangedTodoTaskState : IAction
        {
            public string taskName { get; set; }
            public bool taskState { get; set; }
        }


    }

    /// <summary>
    /// This is a cass for Reducers
    /// </summary>
    /// 
    public static class Reducers
    {
        public static State Reduce(State previousState, IAction action)
        {
            if (action is Action.Increment)
            {
                return Increment(previousState, (Action.Increment)action);
            }

            if (action is Action.Decrement)
            {
                return Decrement(previousState, (Action.Decrement)action);
            }

            if (action is Action.AddTodo)
            {
                return AddTodo(previousState, (Action.AddTodo)action);
            }

            if(action is Action.DeleteTodo)
            {
                return DeleteTodo(previousState, (Action.DeleteTodo)action);
            }

            if (action is Action.ChangedTodoTaskState)
            {
                return ChangedTodoTaskState(previousState, (Action.ChangedTodoTaskState)action);
            }

            if (action is Action.ChangeListViewState)
            {
                return ChangeListViewState(previousState, (Action.ChangeListViewState)action);
            }

            return previousState;
        }


        /// <summary>
        /// State changes for action reducers
        /// </summary>
        /// <param name="previousState"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static State Increment(State previousState, Action.Increment action)
        {
            return new State
            {
                Counter = new State.CounterState
                {
                    count = previousState.Counter.count + 1
                }
            };
        }

        public static State Decrement(State previousState, Action.Decrement action)
        {
            return new State
            {
                Counter = new State.CounterState
                {
                    count = previousState.Counter.count - 1
                }
            };
        }

        public static State AddTodo(State previousState, Action.AddTodo action)
        {   
            return new State
            {
                Todo = previousState.Todo,
                todoViewState = previousState.todoViewState
            };
        }

        public static State DeleteTodo(State previousState, Action.DeleteTodo action)
        {
            return new State
            {
                Todo = previousState.Todo,
                todoViewState = previousState.todoViewState
            };
        }

        public static State ChangedTodoTaskState(State previousState, Action.ChangedTodoTaskState action)
        {
            for (int i = 0; i < previousState.Todo.todoItems.Count; i++)
            {
                if (previousState.Todo.todoItems[i].task.Equals(action.taskName))
                {
                    previousState.Todo.todoItems[i].todoStatus = !previousState.Todo.todoItems[i].todoStatus;
                }
            }

            State.TodoState todoState = new State.TodoState
            {
                todoItems = previousState.Todo.todoItems
            };

            return new State
            {
                Todo = todoState,
                todoViewState = previousState.todoViewState
            };
        }

        public static State ChangeListViewState(State previousState, Action.ChangeListViewState action)
        {
            previousState.todoViewState.todoViewStatus = !previousState.todoViewState.todoViewStatus;

            return new State
            {
                Todo = previousState.Todo,
                todoViewState = previousState.todoViewState
            };
        }

    }
}