﻿using System;
using UnityEngine;

namespace Demo.ReduxStore
{
    public static class DemoThunk
    {
        public static IAction AddTodoItem(String taskName)
        {
            return new ThunkAction<State>((Dispatcher dispatcher, Func<State> getState) =>
            { 
                State newState = getState();

                State.TodoItem todoItem = new State.TodoItem();
                todoItem.task = taskName;
                todoItem.todoStatus = false;
                newState.Todo.todoItems.Add(todoItem);

                dispatcher(new Action.AddTodo { });
            });
        }

        public static IAction DeleteTodoItem(string taskName)
        {
            return new ThunkAction<State>((Dispatcher dispatcher, Func<State> getState) =>
            {
                State newState = getState();
                for (int i = 0; i < newState.Todo.todoItems.Count; i++)
                {
                    if (newState.Todo.todoItems[i].task.Equals(taskName))
                    {
                        newState.Todo.todoItems.Remove(newState.Todo.todoItems[i]);
                    }
                }

                dispatcher(new Action.DeleteTodo { });
            });
        }

    }
}