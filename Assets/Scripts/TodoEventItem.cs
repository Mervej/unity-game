﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace Demo.ReduxStore
{
    public class TodoEventItem : MonoBehaviour
    {

        /// <summary>
        /// This script is for the individual items!!!!
        /// Deletion and completion checks
        /// </summary>

        [SerializeField]
        private Text todoItemText;
        [SerializeField]
        private Toggle todoEventItem;
        [SerializeField]
        private Button deleteTodoEventButton;

        private void Start()
        {
            todoEventItem.onValueChanged.AsObservable()
                .Subscribe(_ =>
                {
                    AddTodo.TodoStore.Dispatch(
                          new Action.ChangedTodoTaskState
                          {
                              taskName = todoItemText.text
                          }
                      );
                }
                );

            deleteTodoEventButton.OnClickAsObservable()
                .Subscribe(_=>
                {
                    AddTodo.TodoStore.Dispatch(DemoThunk.DeleteTodoItem(todoItemText.text));
                        /*
                        new Action.DeleteTodo
                        {
                            taskName = todoItemText.text
                        }
                        );
                        */
                });

        }
    }
}