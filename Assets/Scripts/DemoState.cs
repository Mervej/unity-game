﻿using System.Collections.Generic;

namespace Demo.ReduxStore
{
    public class State
    {
        public TodoViewState todoViewState { get; set; }

        public TodoState Todo { get; set; }

        public CounterState Counter { get; set; }


        /*
        public State initialize()
        {
            return new State
            {
                Counter = new CounterState
                {
                    count = 0
                }
            };
        }
        */

        public State initialize()
        {
            State.TodoItem todoItem = new State.TodoItem();
            todoItem.task = "";
            todoItem.todoStatus = false;
            List<TodoItem> todoListItem = new List<TodoItem>();
            todoListItem.Add(todoItem);
            State.TodoState todoState = new State.TodoState
            {
                todoItems = todoListItem
            };

            return new State
            {
                todoViewState = new TodoViewState
                {
                    todoViewStatus = false
                },
                Todo = new TodoState
                {

                }
            };
        }


        public class CounterState
        {
            public int count;
        }

        public class TodoItem
        {
            public string task;
            public bool todoStatus;
        }

        public class TodoState
        {
            public List<TodoItem> todoItems = new List<TodoItem>();
        }

        public class TodoViewState
        {
            public bool todoViewStatus;
        }
    }
}