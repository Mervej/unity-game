﻿using System;
using UniRx;
using UnityEditor.VersionControl;
using Livelike.ReduxContainer;

namespace Demo.ReduxStore
{

    /// <summary>
    /// This is a midderware defination trying to implement a thunk middleware service
    /// </summary>

    public static class Middleware
    {
        public static Func<Dispatcher, Dispatcher> ThunkMiddleware<TState>(IStore<TState> store)
        {
            return (Dispatcher next) => (IAction action) =>
            {
                var thunkAction = action as ThunkAction<TState>;
                if (thunkAction != null)
                {
                    thunkAction.Action(store.Dispatch, store.GetState);
                    return thunkAction;
                }
                return next(action);
            };
        }
    }

    public class ThunkAction<TState> : IAction
    {
        public Action<Dispatcher, Func<TState>> Action { get; }

        public ThunkAction(Action<Dispatcher, Func<TState>> action)
        {
            this.Action = action;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TState"></typeparam>
    /// <param name="dispatcher"></param>
    /// <param name="getState"></param>
    /// <returns></returns>

    public delegate Task AsyncActionsCreator<TState>(Dispatcher dispatcher, Func<TState> getState);

    public static class StoreExtensions
    {
        public static Task Dispatch<TState>(this IStore<TState> store, AsyncActionsCreator<TState> actionsCreator)
        {
            return actionsCreator(store.Dispatch, store.GetState);
        }
    }

    public delegate IAction Dispatcher(IAction action);

    public delegate TState Reducer<TState>(TState previousState, IAction action);

    public delegate Func<Dispatcher, Dispatcher> Middleware<TState>(IStore<TState> store);

    public interface IStore<TState> : IObservable<TState>
    {
        IAction Dispatch(IAction action);

        TState GetState();
    }

    public interface IState
    {

    }

    public class Store<TState> : IStore<TState>
    {
        private readonly object _syncRoot = new object();
        private readonly Dispatcher _dispatcher;
        private readonly Reducer<TState> _reducer;
        private readonly ReplaySubject<TState> _stateSubject = new ReplaySubject<TState>(1);
        private TState _lastState;
        private Func<State, DemoContainer.IAction, State> combineReducers;
        private State initialState;

        public Store(Func<State, DemoContainer.IAction, State> combineReducers, State initialState)
        {
            this.combineReducers = combineReducers;
            this.initialState = initialState;
        }

        public Store(Reducer<TState> reducer, TState initialState = default(TState), params Middleware<TState>[] middlewares)
        {
            _reducer = reducer;
            _dispatcher = ApplyMiddlewares(middlewares);

            _lastState = initialState;
            _stateSubject.OnNext(_lastState);
        }

        public IAction Dispatch(IAction action)
        {
            return _dispatcher(action);
        }

        public TState GetState()
        {
            return _lastState;
        }

        public IDisposable Subscribe(IObserver<TState> observer)
        {
            return _stateSubject
                .Subscribe(observer);
        }

        private Dispatcher ApplyMiddlewares(params Middleware<TState>[] middlewares)
        {
            Dispatcher dispatcher = InnerDispatch;
            foreach (var middleware in middlewares)
            {
                dispatcher = middleware(this)(dispatcher);
            }
            return dispatcher;
        }

        private IAction InnerDispatch(IAction action)
        {
            lock (_syncRoot)
            {
                _lastState = _reducer(_lastState, action);
            }
            _stateSubject.OnNext(_lastState);
            return action;
        }
    }
}