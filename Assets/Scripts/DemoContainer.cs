﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UniRx;
using Demo.ReduxStore;
namespace Livelike.ReduxContainer
{
    public class DemoContainer : MonoBehaviour
    {

        public interface IAction { }
        public static IStore<State> Store { get; private set; }

        [SerializeField]
        private Button incrementButton;
        [SerializeField]
        private Button decrementButton;
        [SerializeField]
        private Text count;
        [SerializeField]
        private Button addTodo;
        /*
        State initialState = new State { }.initialize();

        private void Awake()
        {
            Store = new Store<State>(CombineReducer, initialState);
        }

        private State CombineReducer(State previousState, IAction action)
        {
            return new State
            {
                Counter = Reducers.Reduce(previousState, action).Counter,
            };
        }

        void Start()
        {
            incrementButton.onClick.AsObservable()
                .Subscribe(_ => Store.Dispatch(new Action.Increment { }));
            decrementButton.onClick.AsObservable()
                .Subscribe(_ => Store.Dispatch(new Action.Decrement { }));

            Store
            .Subscribe(Store =>
                {
                    count.text = Store.Counter.count.ToString();
                }
            );


            addTodo.onClick.AsObservable()
                .Subscribe(_ => SceneManager.LoadScene("AddTodo"));
        }
    
    */
    }
}
