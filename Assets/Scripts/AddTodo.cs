﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace Demo.ReduxStore
{
    public class AddTodo : MonoBehaviour
    {
        //public static IStore<State> TodoStore { get; private set; }

        [SerializeField]
        private Toggle todoCheck;
        [SerializeField]
        private Toggle todoViewStatusButton;
        [SerializeField]
        private InputField task;
        [SerializeField]
        private Text taskName;
        [SerializeField]
        private Transform parentPosition;
        private Vector3 tempPosition;
        /// <summary>
        /// Creating a TodoStore to store all the various states
        /// Combine reducers hass been 
        /// </summary>
        public static Store<State> TodoStore;

        State initialize = new State { }
            .initialize();

        private void Awake()
        {
            TodoStore = new Store<State>(Reducers.Reduce, initialize , Middleware.ThunkMiddleware);
            //TodoStore = new Store<State>(CombineReducers, initialize, Middleware.ThunkMiddleware);
        }
        /*
        private State CombineReducers(State previousState, IAction action)
        {
            return new State
            {
                Todo = Todo.Reducers.Reduce(previousState, action).Todo,
                todoViewState = Reducers.Reduce(previousState, action).todoViewState
            };
        }
        */
        private void Start()
        {
            task.onEndEdit.AsObservable()
                .Subscribe(_ =>
                    TodoStore.Dispatch(DemoThunk.AddTodoItem(task.text)));
            /*
            new Action.AddTodo
            {
                taskname = task.text
            }
            )
            );
            */

            todoViewStatusButton.onValueChanged.AsObservable()
                .Subscribe(store=>
                    {
                        TodoStore.Dispatch(new Action.ChangeListViewState { });
                    }
                );

            TodoStore
                //.Where(store => store.Todo.todoItems != null)
                .Subscribe(Store =>
                    {
                        Debug.Log("rest assure task list view State is " + Store.todoViewState.todoViewStatus);

                        foreach (Transform child in parentPosition)
                        {
                            GameObject.Destroy(child.gameObject);
                        }

                        tempPosition = parentPosition.position;
                        int length = Store.Todo.todoItems.Count;

                        for (int i = 0; i < length; i++)
                        {
                            if(Store.todoViewState.todoViewStatus)
                            {
                                if(!Store.Todo.todoItems[i].todoStatus)
                                    InstantiatePrefab(Store.Todo.todoItems[i].task,Store.Todo.todoItems[i].todoStatus);
                            }
                            else
                            {
                                InstantiatePrefab(Store.Todo.todoItems[i].task,Store.Todo.todoItems[i].todoStatus);
                            }
                        }
                    }
                );
        }


        private void InstantiatePrefab(string taskNameText, bool taskStatus)
        {
            tempPosition = tempPosition + new Vector3(0, -30f, 0);
            taskName.text = taskNameText;
            todoCheck.isOn = taskStatus;
            Instantiate(todoCheck, tempPosition, new Quaternion(0, 0, 0, 0), parentPosition);
        }
    }
}